How to Create a Secure Session Management System in PHP and MySQL
=================================================================

I want a PHP Session management class that encrypts sessions and stores them in a MySQL database.  Preferrably I would like to use one that is well-supported.  It appears from Google search that the most commonly referenced session management system is:

https://www.wikihow.com/Create-a-Secure-Session-Management-System-in-PHP-and-MySQL

I would like you to implement this session management system using the latest production version of PHP (version 7.0.25) and MySQL (version 5.7.21) running on the latest LTS version of Ubuntu (16.04).

I have a dedicated cloud server set up with such an implementation (which you can download using git clone https://stevenjgarner@bitbucket.org/stevenjgarner/wikihow_sessions.git), and it comes up with the following error (see http://192.237.247.136/index.php):

	Warning: Uncaught mysqli_sql_exception: MySQL server has gone away in /var/www/html/library/web_sess.class.php:76 
	Stack trace: 
	#0 /var/www/html/library/web_sess.class.php(76): mysqli_stmt->execute() 
	#1 [internal function]: web_sess->read('d1dn807bknqfea2...') 
	#2 /var/www/html/library/web_sess.class.php(55): session_regenerate_id(true) 
	#3 /var/www/html/index.php(23): web_sess->start_session('_s', false) 
	#4 {main} thrown in /var/www/html/library/web_sess.class.php on line 76

	Catchable fatal error: session_regenerate_id(): Failed to create(read) session ID: user (path: /var/lib/php/sessions) in /var/www/html/library/web_sess.class.php on line 55

Reloading this page multiple times will eventually come up with:

	Warning: session_start(): Failed to decode session object. Session has been destroyed in /var/www/html/library/web_sess.class.php on line 52

	Warning: session_regenerate_id(): Cannot regenerate session id - session is not active in /var/www/html/library/web_sess.class.php on line 55

I cannot figure out what is going on with this.

I actually have the wikihow system set up on an older server where it IS working ... well kind of:  the gc (garbage collector) function does not work so I have rapidly accumulated a large number (millions) of records in the MySQL database, which I have to manually delete.

The 16.04 LTS Ubuntu instance I am using is on Rackspace and the only setup that has happened is:

	apt-get update
	apt-get upgrade
	sudo apt-get install apache2
	sudo apt-get install mysql-server
	sudo apt-get install -y php7.0 libapache2-mod-php7.0 php7.0-cli php7.0-common php7.0-mbstring php7.0-gd php7.0-intl php7.0-xml php7.0-mysql php7.0-mcrypt php7.0-zip
	sudo systemctl restart apache2.service

Then I installed the above (git clone https://stevenjgarner@bitbucket.org/stevenjgarner/wikihow_sessions.git) into the /var/www/html/ apache2 web root directory.

Steven Garner
stevenjgarner@gmail.com

2018-02-10

<?php

class db
{
  function __construct()
  {

  }

  function db_connection($config)
  {
    include($config);
    //mysql_report(mysql_REPORT_ERROR | mysql_REPORT_STRICT);
     
    $mysql = new mysql($host, $user, $password, $database);

    // Constants do not allow multiple config's per website
    // $mysql = new mysql(constant('HOST'), constant('USER'), constant('PASSWORD'), constant('DATABASE'));
   
    if ($mysql->connect_errno) {
      echo "Failed to connect to MySQL: " . $mysql->connect_error;
    }
    return $mysql;
  }

  function db_query($mysql, $sql)
  {
    $result = $mysql->query($sql);
    return $result;
    $result->free();
  }

  function db_result($config, $sql)
  {
    // echo $sql;
    $mysql = $this->db_connection($config);
    $result = $this->db_query($mysql, $sql);
    $mysql->close();
    return $result;
    // $result->free();
  }

  function db_insert($mysql, $sql)
  {
    if ($mysql->query($sql) === TRUE) {
      $last_id = $mysql->insert_id;
    } else {
      echo "Error: " . $sql . "<br>" . $mysql->error;
    }
    return $last_id;
  }

  function db_insert_into($config, $sql)
  {
    $mysql = $this->db_connection($config);
    return $last_id = $this->db_insert($mysql, $sql);
  }

  function db_new_record($config, $sql)
  {
    $mysql = $this->db_connection($config);
    $last_id = $this->db_insert($mysql, $sql);
    $mysql->close();
    return $last_id;
  }

  function db_record($config, $sql)
  {
    $result = $this->db_result($config, $sql);
    $row = $result->fetch_assoc();
    $result->free();
    return $row;
  }

  function db_list($config, $sql, $file)
  {
    //if ($result = $this->db_result($config, $sql)) {
      $result = $this->db_result($config, $sql);
      /* fetch associative array */
      $counter = 0; // set counter
      $c = true; // initialize our variables
      while ($row = $result->fetch_assoc()) {
        $counter++; // or $counter = $counter + 1;
        // alternating row colors
        $css_class = (($c = !$c)?'even':'odd');
        include("$file");
      }
      /* free result set */
      //$result->free();
      return $counter;
    // }
  }

  function db_list_arr($arr)
  {
    $config = $arr[0];
    $sql = $arr[1];
    $file = $arr[2];
    $pth = $arr[3];
    $result = $this->db_result($config, $sql);
    /* fetch associative array */
    $counter = 0; // set counter
    $c = true; // initialize our variables
    while ($row = $result->fetch_assoc()) {
      $counter++; // or $counter = $counter + 1;
      // alternating row colors
      $css_class = (($c = !$c)?'even':'odd');
      include($pth."$file");
    }
    return $counter;
  }

  function db_list_site($arr)
  {
    $config = $arr[0];
    $sql = $arr[1];
    $file = $arr[2];
    $site = $arr[3];
    $result = $this->db_result($config, $sql);
    /* fetch associative array */
    $counter = 0; // set counter
    $c = true; // initialize our variables
    while ($row = $result->fetch_assoc()) {
      $counter++; // or $counter = $counter + 1;
      // alternating row colors
      $css_class = (($c = !$c)?'even':'odd');
      include($site.$file);
    }
    return $counter;
  }

  function db_drop_menu($config, $sql, $file, $selected)
  {
    //if ($result = $this->db_result($config, $sql)) {
      $result = $this->db_result($config, $sql);
      /* fetch associative array */
      $counter = 0; // set counter
      $c = true; // initialize our variables
      while ($row = $result->fetch_assoc()) {
        $counter++; // or $counter = $counter + 1;
        // alternating row colors
        $css_class = (($c = !$c)?'even':'odd');
        include("$file");
      }
      /* free result set */
      //$result->free();
      return $counter;
    // }
  }

  function drop($config, $name, $default, $drop_menu_type, $site)
  {
    // HTML Select Drop Menu
    $var = "<select name='".$name."'>";
    include_once($site."model/drop-".$drop_menu_type.".php");
    $file = $site."view/templates/drop-".$drop_menu_type.".php";
    if ($result = $this->db_result($config, $sql)) {
      /* fetch associative array */
      while ($row = $result->fetch_assoc()) {

        if($default == $row["id"])
        {
          $selected = " selected='selected'";
        }
        else
        {
          $selected = '';
        }

        include("$file");
      }
      /* free result set */
      $result->free();
    $var .= "</select>";
    return $var;
    }
  }

  function state($config, $name, $default, $drop_menu_type)
  {
    // HTML Select Drop Menu
    $var = "<select name='".$name."'>";
    include_once("model/drop-".$drop_menu_type.".php");
    $file = "view/templates/drop-".$drop_menu_type.".php";
    if ($result = $this->db_result($config, $sql)) {
      /* fetch associative array */
      while ($row = $result->fetch_assoc()) {

        if($default == $row["key"])
        {
          $selected = " selected='selected'";
        }
        else
        {
          $selected = '';
        }

        include("$file");
      }
      /* free result set */
      $result->free();
    $var .= "</select>";
    return $var;
    }
  }

  function escape($config, $escapestr)
  {
    $link = $this->db_connection($config);
    return mysql_real_escape_string($link, $escapestr);
  }

  function sanitize($config, $data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    $data = $this->escape($config, $data);
    return $data;
  }

}

?>
